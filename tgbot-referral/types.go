package main

import (
	"time"

)

//KeyCode - структура токена
type KeyCode struct {
	ID uint64 
	Key string `gorm:"not null"`
	CreatedAt time.Time
}

//TableName название таблицы для токенов
func (k *KeyCode) TableName() string {
	return "invite_keys"
}

// User - структура пользователя
type User struct {
	ID int64
	TelegramID int64
	InvitedID int64
	KeyID uint64
	Tokens float64
	CreatedAt time.Time
}

// TokenConfig - структура настроек токена
type TokenConfig struct {
	Users float64
	Coefficient float64
	Accrual float64
	Price float64
}