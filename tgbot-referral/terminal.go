package main

import (
	"strconv"
	"os"
	"fmt"
	"bufio"
	"strings"
	"time"
	"log"
)

type token struct {
	key int
	value string
}

const (
	isCommand = 10
	isWord = 0
)

const (
	keyTest = "test"
	keyHelp = "help"
	keyCreateUser = "createuser"
	keyCreateKey = "createkey"
	keyReKey = "rekey"
	keyUpdateConfig = "updateconfig"
)

var keywords = map[string]int {
	keyHelp : isCommand,
	keyTest : isCommand,
	keyCreateUser : isCommand,
	keyCreateKey : isCommand,
	keyReKey : isCommand,
	keyUpdateConfig : isCommand,
}

var tokens []token
var isBusy bool
var indexBusy int

// Listen обрабатывает запросы через терминал
func (bot *Bot) Listen() {
	for {
		var commands []string
		myscanner := bufio.NewScanner(os.Stdin)
		myscanner.Scan()
		command := myscanner.Text()
		commands = strings.Split(command, " ")
		tokens = []token{}
		lexer(commands)
		lenTokens := len(tokens)
		for i, t := range(tokens) {
			if !isBusy && (t.key == isWord) {
				continue
			}
			if (t.key == isCommand && !isBusy) && lenTokens > 1 {
				isBusy = true
				indexBusy = i
			}
			if t.key == isWord || (lenTokens == 1 && t.key == isCommand) {
				switch(tokens[indexBusy].value) {
				case keyTest:
					if len(commands) > i {
						for j := indexBusy+1; j < len(commands); j++ {
							fmt.Println(tokens[j].value)
						}
					}
				case keyHelp:
					fmt.Println("[help]: createuser <telegram id> <invited id> <key id> <tokens>")
					fmt.Println("[help]: createkey <key or gen>")
					fmt.Println("[help]: rekey <key id>")
					fmt.Println("[help]: updateconfig")
				case keyCreateKey:
					if len(commands) == 2 {
						var keyString string
						if commands[i] == "gen" {
							keyString = GetHashKey(time.Now().UnixNano())
						} else {
							keyString = commands[i]
						}
						key := KeyCode {
							Key: keyString,
							CreatedAt: time.Unix(0, time.Now().UnixNano()),
						}
						bot.db.Create(&key)
						log.Printf("[createkey] Ключ %s создан. ID: %d\n", keyString, key.ID)						
					}
				case keyCreateUser:
					if len(commands) == i+4 {
						tID, _ := strconv.ParseInt(commands[i], 10, 64)
						iID, _ := strconv.ParseInt(commands[i+1], 10, 64)
						keyID, _ := strconv.ParseUint(commands[i+2], 10, 64)
						tokens, _ := strconv.ParseFloat(commands[i+3], 64)
						user := User{							
							TelegramID: tID,
							InvitedID: iID,
							KeyID: keyID,
							CreatedAt: time.Unix(0, time.Now().UnixNano()),
							Tokens: tokens,
						}
						bot.db.Create(&user)
						log.Printf("[createuser]: Пользователь создан. ID: %d\n", user.ID)
					}
				case keyReKey:
					if len(commands) == 2 {
						var key KeyCode						 
						bot.db.Where("`id` = ?", commands[i]).First(&key)
						if key.CreatedAt.IsZero() {
							log.Printf("[rekey] Ключ %s не существует\n", commands[i])
							continue
						}
						key.Key = GetHashKey(time.Now().UnixNano())
						key.CreatedAt = time.Unix(0, time.Now().UnixNano())
						bot.db.Save(&key)
						log.Printf("[rekey] Ключ %s имеет новое значение: %s\n", commands[i], key.Key)
					}
				case keyUpdateConfig:
					bot.UpdateConfig()
					log.Printf("[updateconfig] Статистика токенов обновлена")
				}
			}
		}
	}
}

func lexer(commands []string) {
	for _, command := range(commands) {
		if id, ok := keywords[command]; ok {
			tokens = append(tokens, token{id, command})
		} else {
			tokens = append(tokens, token{isWord, command})
		}
	}
}