package main

import (
    "crypto/md5"
	"encoding/hex"
	"time"
	"strconv"
	"fmt"
	"math"
)

// GetHashKey генерирует универсальный прикласительный ключ
func GetHashKey(id int64) string {
	hasher := md5.New()
	ID := strconv.FormatInt(id, 10)
	Time := strconv.FormatInt(time.Now().UnixNano(), 10)
    hasher.Write([]byte(ID + ".looper." + Time))
    return hex.EncodeToString(hasher.Sum(nil))
}

// UpdateHashKey генерирует новый пригласительный код, если с последней генерации прошло больше суток
func (bot *Bot) UpdateHashKey(userID int64, key KeyCode) {
	if key.CreatedAt.Unix() + 43200 < time.Now().Unix() {
		key.Key = GetHashKey(userID)
		key.CreatedAt = time.Unix(0, time.Now().UnixNano())
		bot.db.Save(&key)
	}
}

// GetProfileInfo возвращает строку со статистикой пользователя user
func (bot *Bot) GetProfileInfo(user User) string {
	var (
		messageText string
		countUsers uint64 // Количество всех пользователей
		countInvited uint64 // Количество приглашенных пользователей
		tokenConfig TokenConfig
	)

	bot.db.Model(&User{}).Count(&countUsers)
	bot.db.Find(&tokenConfig)
	bot.db.Model(&User{}).Where("`invited_id` = ?", user.ID).Count(&countInvited)
	
	messageText = "Всего зарегистрировано: `" + strconv.FormatUint(countUsers, 10) + "`\n"
	messageText += fmt.Sprintf("\nПоследний приглашенный получил `%.2f` токенов\n", tokenConfig.Accrual)
	messageText += fmt.Sprintf("Текущая стоимость токена: `%.2f$`\n\n", tokenConfig.Price)

	messageText += fmt.Sprintf("Ваш баланс: `%.2f` токенов (`%.2f$`)\n", user.Tokens, user.Tokens * tokenConfig.Price)
	messageText += "Вы пригласили: `" + strconv.FormatUint(countInvited, 10) + "`"

	messageText += "\n\nКод приглашения:\n\n"

	return messageText
}

// GetKeyUser возвращает пригласительный ключ пользователя из БД
func (bot *Bot) GetKeyUser(user User) string {
	var (
		key KeyCode
		messageText string
	)
	bot.db.Where("`id` = ?", user.KeyID).First(&key)
	bot.UpdateHashKey(user.ID, key)
	if key.Key == "" {
		messageText = "Сегодня код был использован"
	} else {
		messageText = "`" + key.Key + "`"
	}

	return messageText		
}

//UpdateConfig обновляет информацию о токенах
func (bot *Bot) UpdateConfig() *TokenConfig{
	var (
		tokenConfig TokenConfig
		counts float64
	)
	bot.db.Model(&User{}).Count(&counts)
	bot.db.Delete(&tokenConfig)
	tokenConfig.Users = counts
	tokenConfig.Coefficient = math.Log10(counts)
	tokenConfig.Accrual = 100 / tokenConfig.Coefficient
	tokenConfig.Price = 10 / tokenConfig.Accrual
	bot.db.Save(&tokenConfig)
	return &tokenConfig
}