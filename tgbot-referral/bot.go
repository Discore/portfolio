package main

import (
	"log"
	"time"
	"fmt"
	"strconv"
	"os"

	"github.com/jinzhu/gorm"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/360EntSecGroup-Skylar/excelize"
)

// Bot - главная структура бота, через которую происходит вся работа
type Bot struct {
	bot *tgbotapi.BotAPI
	db *gorm.DB
}

//NewBot создает указатель на структуру Bot
func NewBot(bot *tgbotapi.BotAPI, db *gorm.DB) *Bot {
	return &Bot{
		bot: bot,
		db: db, 
	}
}

// UpdateDB обновляет таблицу, если были сделаны изменения в структуре types.go
func (update *Bot) UpdateDB() {
	update.db.AutoMigrate(KeyCode{}, User{}, TokenConfig{})
}

// Run запускает бота
func (robot *Bot) Run() {
	log.Println("Run")
	robot.bot.Debug = false
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, _ := robot.bot.GetUpdatesChan(u)
	for update := range updates {
		if update.Message == nil {
			continue
		}
		if update.Message.Text == "" {
			continue
		}
		if update.Message.ForwardFrom != nil || update.Message.ForwardFromChat != nil {
			continue
		}
		if update.Message.Entities == nil {
			//если первый символ "." - это команда
			if update.Message.Text[0] == '.' {
				var user User
				robot.db.Where("`telegram_id` = ?", update.Message.Chat.ID).First(&user)	
				if user.CreatedAt.IsZero() {
					continue
				}
				switch(update.Message.Text) {
				default:	
					msg := tgbotapi.NewMessage(user.TelegramID, robot.GetProfileInfo(user))
					msg.ParseMode = "Markdown"
					robot.bot.Send(msg)
					msg = tgbotapi.NewMessage(user.TelegramID, robot.GetKeyUser(user))
					msg.ParseMode = "Markdown"
					robot.bot.Send(msg)
				case ".export":
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Создание excel-файла...")
					robot.bot.Send(msg)
					filename := "export" + strconv.FormatInt(update.Message.Chat.ID, 10) + ".xlsx"
					go func (){		
						xlsx := excelize.NewFile()
						index := xlsx.NewSheet("Users")
						xlsx.SetActiveSheet(index)
						xlsx.SetCellValue("Users", "A1", "ID")
						xlsx.SetCellValue("Users", "B1", "Telegram ID")
						xlsx.SetCellValue("Users", "C1", "Invited ID")
						xlsx.SetCellValue("Users", "D1", "Key ID")
						xlsx.SetCellValue("Users", "E1", "Created At")
						xlsx.SetCellValue("Users", "F1", "Tokens")
						xlsx.SetColWidth("Users", "A", "F", 20)

						var (
							users []User
							countUsers int64 = 2
						)

						robot.db.Find(&users, &users)

						for _, u := range(users) {
							xlsx.SetCellValue("Users", "A" + strconv.FormatInt(countUsers, 10), strconv.FormatInt(u.ID, 10))
							xlsx.SetCellValue("Users", "B" + strconv.FormatInt(countUsers, 10), strconv.FormatInt(u.TelegramID, 10))
							xlsx.SetCellValue("Users", "C" + strconv.FormatInt(countUsers, 10), strconv.FormatInt(u.InvitedID, 10))
							xlsx.SetCellValue("Users", "D" + strconv.FormatInt(countUsers, 10), strconv.FormatUint(u.KeyID, 10))
							xlsx.SetCellValue("Users", "E" + strconv.FormatInt(countUsers, 10), u.CreatedAt.Add(time.Duration(3)*time.Hour).Format("02.01.2006 15:04:05"))
							xlsx.SetCellValue("Users", "F" + strconv.FormatInt(countUsers, 10), fmt.Sprintf("%.2f", u.Tokens))
							countUsers++
						}

						err := xlsx.SaveAs(filename)
						if err != nil {
							log.Println(err.Error())
							msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Ошибка в экспорте файла. Обратитесь к разработчику для дополнительной информации")
							robot.bot.Send(msg)
						}
	
						msg := tgbotapi.NewDocumentUpload(update.Message.Chat.ID, filename)
						_, err = robot.bot.Send(msg)
						
						os.Remove(filename)
					}()					
				}
			} else {
				var key KeyCode
				robot.db.Where("`key` = ?", update.Message.Text).First(&key)
				if !key.CreatedAt.IsZero() {
					var user User
					robot.db.Where("`telegram_id` = ?", update.Message.Chat.ID).First(&user)
					if user.CreatedAt.IsZero() {
						robot.db.Where("`key_id` = ?", key.ID).First(&user)	
						localUser := User{
							TelegramID: update.Message.Chat.ID,
							InvitedID: user.ID, 
							KeyID: 0,
							CreatedAt: time.Unix(0, time.Now().UnixNano()),
						}
						robot.db.Create(&localUser)
						
						//robot.db.First(&localUser, &localUser)
						localKey := KeyCode{
							Key: GetHashKey(localUser.ID),
							CreatedAt: time.Unix(0, time.Now().UnixNano()),
						}
						robot.db.Create(&localKey)

						localUser.KeyID = localKey.ID

						var	tokenConfig TokenConfig

						tokenConfig = *robot.UpdateConfig()
						user.Tokens += tokenConfig.Accrual
						localUser.Tokens += tokenConfig.Accrual						

						robot.db.Save(&localUser)
						robot.db.Save(&user)

						robot.db.Model(&key).Update("key", "")

						msg := tgbotapi.NewMessage(update.Message.Chat.ID, robot.GetProfileInfo(localUser))
						msg.ParseMode = "Markdown"
						robot.bot.Send(msg)		
						msg = tgbotapi.NewMessage(update.Message.Chat.ID, robot.GetKeyUser(localUser))
						msg.ParseMode = "Markdown"
						robot.bot.Send(msg)				
					}
					
				} else {
					//если код неверный
				}				
			}
		} else {
			continue
		}
	}
}