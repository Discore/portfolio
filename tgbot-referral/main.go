package main

import (
	"log"
	
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/yanzay/cfg"
)

func main() {
	log.Println("Reading the configuration file")
	cfgInfo := make(map[string]string)
	err := cfg.Load("bot.cfg", cfgInfo)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("The connection to the database")
	db, err := gorm.Open(cfgInfo["db_server"], cfgInfo["db_user"] + ":" + cfgInfo["db_password"] + "@/" + cfgInfo["db_name"] + "?charset=utf8mb4&parseTime=true")
	defer db.Close()
	if err != nil {
		log.Panic(err)
	}
	
	log.Println("Connecting to API Telegram Bot")
	botAPI, err := tgbotapi.NewBotAPI(cfgInfo["token_bot"])
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Authorized on account @%s", botAPI.Self.UserName)

	bot := NewBot(botAPI, db)
	
	bot.UpdateDB()
	
	go bot.Listen()
	bot.Run()
	
}